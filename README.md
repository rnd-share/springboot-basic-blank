# springboot-basic

## Git
- [ ] git clone


## TODO

- [ ] Enable Annotation Processing
- [ ] Plugin lombok
- [ ] build using maven
- [ ] annotation, component
- [ ] properties
- [ ] ResponseSchema entity
- [ ] Response Entity ok , ResponseEntityOf, ResponseEntityOfNullable 
- [ ] pom.xml custom jar name 
- [ ] perbedaan war & jar
- [ ] lombok vs Vanilla
- [ ] version di dependencies
- [ ] perbedaan DTO, Entity
- [ ] pagination / https://docs.spring.io/spring-data/jpa/reference/repositories/core-concepts.html
- [ ] JpaRepository vs CrudRepository
- [ ] Quarkus
- 
- 

## JPARepository
https://dev.to/tienbku/jpa-repository-query-example-in-spring-boot-derived-query-3c38

## Usecase

- [ ] CRUD Employee
- [ ] Connect to Database


## Docker build

### build with tag version
```
podman build -f "Dockerfile" -t "springboot-basic:1.0.0"  "."
```

### build with tag latest version
```
podman build -f "Dockerfile" -t "springboot-basic:latest"  "."
```

### run docker
```
podman run springboot-basic:latest  
```

### run docker in background
```
podman run springboot-basic:latest -d  
```

### run docker with name container & port maping
```
podman run -p [host_port]:[container_port] --name [container_name]  springboot-basic:latest 
podman run -p 8080:8080 --name springboot springboot-basic:latest 
```


