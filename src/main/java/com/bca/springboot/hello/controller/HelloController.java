package com.bca.springboot.hello.controller;

import com.bca.springboot.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class HelloController {

	@Autowired
	private HelloService service;

	@GetMapping("hello")
	public String hello() {
		return "Hello World";
	}

	@GetMapping("{name}")
	public String hello(@PathVariable("name") String name) {
		return "Hello " + name;
	}

	@GetMapping("param")
	public String helloWithParam(@RequestParam("name") String name) {
		return "Hello " + name;
	}

	@GetMapping("service")
	public String helloFromService() {
		return "Hello " + service.getName();
	}



}
