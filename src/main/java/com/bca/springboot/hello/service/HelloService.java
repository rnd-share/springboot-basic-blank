package com.bca.springboot.hello.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {

	public String getName() {
		return "World";
	}

}
